###1. 基本配置
  1. 前台运行配置
    ``
    daemon off;
    ``
###2. 负载均衡
 ```aidl
    http {
        upstream webs {
            server web2.dataprajna.com:8022;
            server yk.ytc6.com:20290;
            server ss.dataprajna.com:31025;
        }
        server {
                listen       80;
                server_name  localhost;
                location / {
                    proxy_pass http://webs;
                }
        }
    }
    
```