###1. 启动keepalived
```
    service keepalived start # 启动
    service keepalived status # 查看状态
```
###2. keepalived.conf配置
 ```
    global_defs {
       router_id LVS_01 #全局唯一 可类比 mysql主从时server Id
    }
    
    vrrp_instance VI_1 {
        state MASTER # 主
        interface enp0s3 # 网卡名称
        virtual_router_id 51 
        priority 150 # 主 的话可以设置大一点
        advert_int 1        
        authentication {
            auth_type PASS
            auth_pass 1111
        }
        virtual_ipaddress {
            192.168.1.12  # vip（虚拟ip）
        }
    }
```